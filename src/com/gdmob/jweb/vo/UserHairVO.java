package com.gdmob.jweb.vo;

public class UserHairVO {
private String userid;
private String hairid;
private String deviceid;
public String getUserid() {
	return userid;
}
public void setUserid(String userid) {
	this.userid = userid;
}
public String getHairid() {
	return hairid;
}
public void setHairid(String hairid) {
	this.hairid = hairid;
}
public String getDeviceid() {
	return deviceid;
}
public void setDeviceid(String deviceid) {
	this.deviceid = deviceid;
}
}
