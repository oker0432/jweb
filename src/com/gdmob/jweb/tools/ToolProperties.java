package com.gdmob.jweb.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ToolProperties {
	public static Properties loadProperties(String fullFile){
		FileInputStream inputStream = null;
		Properties p = null;
		try{
			inputStream = new FileInputStream(new File(fullFile));
			p = new Properties();
			p.load(inputStream);
			return p;
		}catch(Exception e){
			return null;
		}finally{
			try{
				if(inputStream != null){
					inputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
