package com.gdmob.jweb.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Table;
import com.gdmob.jweb.tools.ToolSqlXml;

@SuppressWarnings("unused")
@Table(tableName="jw_soft")
public class Soft extends BaseModel<Soft> {
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(Soft.class);
	
	public static final Soft dao = new Soft();
	public Soft findSoftByType(int type){
		String sql = ToolSqlXml.getSql("pingtai.soft.findSoftByType");
		return dao.findFirst(sql,type);
	} 
}
