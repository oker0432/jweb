package com.gdmob.jweb.controller;

import java.io.File;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Controller;
import com.gdmob.jweb.config.Constants;
import com.gdmob.jweb.model.Soft;
import com.gdmob.jweb.model.Upload;
import com.gdmob.jweb.plugin.PropertiesPlugin;
import com.gdmob.jweb.service.SoftService;
import com.jfinal.upload.UploadFile;

@Controller(controllerKey = "/jw/soft")
public class SoftController extends BaseController {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(SoftController.class);
	
	public void list() {
		SoftService.service.list(splitPage);
		render("/pingtai/soft/list.html");
	}
	//保存
	public void uploadSoft(){
		UploadFile uploadFile = getFile("soft.soft");
		String ids = getDestFile(uploadFile,Constants.SOFT);
		Soft soft = getModel(Soft.class,"soft");
		soft.set("soft", ids);
		soft.saveModel();
		redirect("/jw/soft/list");
	}
	public void edit() {
		setAttr("soft", Soft.dao.findById(getPara()));
		//setAttr("colors",MetadataService.service.getMetaDatasByType(MetaData.TYPE_COLOR));
		render("/pingtai/soft/update.html");
	}
	//更新
	public void uploadOtherSoft(){
		UploadFile uploadFile = getFile("soft.soft");
		Soft soft = getModel(Soft.class,"soft");
		SoftService.service.update(uploadFile, soft);
		redirect("/jw/soft/list");
	}
	public void delete(){
		SoftService.service.delete(getPara());
		redirect("/jw/soft/list");
	}
	public void download(){
		int type = getParaToInt("type");
		try{
			Soft soft = Soft.dao.findSoftByType(type);
			String fileId = soft.getStr("soft");
			Upload upload = Upload.dao.findById(fileId,"path");
			String dir = (String) PropertiesPlugin.getParamMapValue(Constants.DATA_DIR);
			File file = new File(dir + File.separator + upload.getStr("path"));
			if(file.exists()){
				renderFile(file);
			}else{
				renderNull();
			}
		}catch(Exception e){
			renderNull();
		}
	}
}
