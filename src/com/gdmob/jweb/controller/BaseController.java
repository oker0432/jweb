package com.gdmob.jweb.controller;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.gdmob.jweb.common.DictKeys;
import com.gdmob.jweb.common.SplitPage;
import com.gdmob.jweb.config.Constants;
import com.gdmob.jweb.model.BaseModel;
import com.gdmob.jweb.model.Upload;
import com.gdmob.jweb.plugin.PropertiesPlugin;
import com.gdmob.jweb.tools.ToolContext;
import com.gdmob.jweb.tools.ToolDateTime;
import com.gdmob.jweb.tools.ToolString;
import com.jfinal.core.Controller;
import com.jfinal.upload.UploadFile;

/**
 * 公共Controller
 */
public abstract class BaseController extends Controller {

	private static Logger log = Logger.getLogger(BaseController.class);
	
	/**
	 * 全局变量
	 */
	protected String ids;			// 主键
	protected SplitPage splitPage;	// 分页封装
	protected List<?> list;			// 公共list
	protected String operatorids;
	protected String userids;
	/**
	 * 请求/WEB-INF/下的视图文件
	 */
	public void toUrl() {
		String toUrl = getPara("toUrl");
		render(toUrl);
	}

	/**
	 * 获取当前请求国际化参数
	 * @return
	 */
	protected String getI18nPram() {
		return getAttr("localePram");
	}

	/**
	 * 获取项目请求根路径
	 * @return
	 */
	protected String getCxt() {
		return getAttr("cxt");
	}
	
	/**
	 * 重写getPara，进行二次decode解码
	 */
	@Override
	public String getPara(String name) {
		String value = getRequest().getParameter(name);
		if(null != value && !value.isEmpty()){
			try {
				value = URLDecoder.decode(value, ToolString.encoding);
			} catch (UnsupportedEncodingException e) {
				log.error("decode异常："+value);
			}
		}
		return value;
	}
	
	/**
	 * 获取checkbox值，数组
	 */
	public String[] getParas(String name) {
		return getRequest().getParameterValues(name);
	}
	
	/**
	 * 判断验证码是否正确
	 * @return
	 */
	protected boolean authCode() {
		String authCodePram = getPara("authCode");
		String authCodeCookie = ToolContext.getAuthCode(getRequest());
		if (null != authCodePram && null != authCodeCookie) {
			authCodePram = authCodePram.toLowerCase();// 统一小写
			authCodeCookie = authCodeCookie.toLowerCase();// 统一小写
			if (authCodePram.equals(authCodeCookie)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 效验Referer有效性
	 * @return
	 */
	protected boolean authReferer() {
		String referer = getRequest().getHeader("Referer");
		if (null != referer && !referer.trim().equals("")) {
			referer = referer.toLowerCase();
			String domainStr = (String) PropertiesPlugin.getParamMapValue(DictKeys.config_domain_key);
			String[] domainArr = domainStr.split(",");
			for (String domain : domainArr) {
				if (referer.indexOf(domain.trim()) != -1) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 判断请求者和数据创建者是否一致
	 * @param entity
	 * @param user
	 * @return
	 */
	protected <T extends BaseModel<?>>  boolean authCreate(T model){
		String createids = model.getStr("createids");
		if(null != createids && !createids.isEmpty()){
			String createUserIds = ToolContext.getCurrentUser(getRequest(), false).getStr("ids");
			if(createids.equals(createUserIds)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 获取查询参数
	 * 说明：和分页分拣一样，但是应用场景不一样，主要是给查询导出的之类的功能使用
	 * @return
	 */
	public Map<String, String> getQueryParam(){
		Map<String, String> queryParam = new HashMap<String, String>();
		Enumeration<String> paramNames = getParaNames();
		while (paramNames.hasMoreElements()) {
			String name = paramNames.nextElement();
			String value = getPara(name);
			if (name.startsWith("_query") && !value.isEmpty()) {// 查询参数分拣
				String key = name.substring(7);
				if(null != value && !value.trim().equals("")){
					queryParam.put(key, value.trim());
				}
			}
		}
		
		return queryParam;
	}

	/**
	 * 设置默认排序
	 * @param colunm
	 * @param mode
	 */
	public void defaultOrder(String colunm, String mode){
		if(null == splitPage.getOrderColunm() || splitPage.getOrderColunm().isEmpty()){
			splitPage.setOrderColunm(colunm);
			splitPage.setOrderMode(mode);
		}
	}
	
	/**
	 * 排序条件
	 * 说明：和分页分拣一样，但是应用场景不一样，主要是给查询导出的之类的功能使用
	 * @return
	 */
	public String getOrderColunm(){
		String orderColunm = getPara("orderColunm");
		return orderColunm;
	}

	/**
	 * 排序方式
	 * 说明：和分页分拣一样，但是应用场景不一样，主要是给查询导出的之类的功能使用
	 * @return
	 */
	public String getOrderMode(){
		String orderMode = getPara("orderMode");
		return orderMode;
	}
	
	public String getOperatorids() {
		return operatorids;
	}

	public void setOperatorids(String operatorids) {
		this.operatorids = operatorids;
	}

	public String getUserids() {
		return userids;
	}

	public void setUserids(String userids) {
		this.userids = userids;
	}

	/************************************		get 	set 	方法		************************************************/

	public void setSplitPage(SplitPage splitPage) {
		this.splitPage = splitPage;
	}
	protected String getDestFile(UploadFile uploadFile,String type){
		String typeDir = type+File.separator+ToolDateTime.getCurrentYmd();
		String dir = (String) PropertiesPlugin.getParamMapValue(Constants.DATA_DIR)+File.separator+typeDir;
		File destDir = new File(dir);
		if(!destDir.exists()){
			destDir.mkdirs();
		}
		String fileName = uploadFile.getFileName();
		if(type.equals(Constants.SOFT)){
			fileName = uploadFile.getOriginalFileName();
		}
		File destFile = new File(destDir,fileName);
		try {
			FileUtils.moveFile(uploadFile.getFile(),destFile);
		} catch (IOException e) {
			
		}
		String filePath = typeDir + File.separator + fileName;
		Upload upload = new Upload();
		upload.set("type",type);
		upload.set("filename",fileName);
		upload.set("contenttype",uploadFile.getContentType());
		upload.set("originalfilename",uploadFile.getOriginalFileName());
		filePath.replace("\\","/");
		upload.set("path",filePath);
		upload.save();
		String ids = upload.getStr("ids");
		return ids;
	}
}
