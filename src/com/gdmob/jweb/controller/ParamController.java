package com.gdmob.jweb.controller;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Controller;
import com.gdmob.jweb.model.Param;
import com.gdmob.jweb.service.ParamService;
import com.gdmob.jweb.tools.ToolSqlXml;
import com.gdmob.jweb.validator.ParamValidator;
import com.jfinal.aop.Before;

@Controller(controllerKey = "/jw/param")
public class ParamController extends BaseController {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(ParamController.class);
	
	public void index() {
		String sql = ToolSqlXml.getSql("pingtai.param.treeTableNodeRoot");
		list = Param.dao.find(sql);
		render("/pingtai/param/treeTable.html");
	}
	
	public void treeTable() {
		String sql = ToolSqlXml.getSql("pingtai.param.treeTableChildNode");
		list = Param.dao.find(sql, ids);
		render("/pingtai/param/treeTableSub.html");
	}

	public void treeData()  {
		String jsonText = ParamService.service.childNodeData(ids);
		renderJson(jsonText);
	}
	
	@Before(ParamValidator.class)
	public void save() {
		ParamService.service.save(getModel(Param.class));
		redirect("/jw/param/toUrl?toUrl=/pingtai/param/treeTableIframe.html");
	}
	
	public void edit() {
		Param param = Param.dao.findById(getPara());
		String pIds = param.getStr("parentids");
		Param parent = Param.dao.findById(pIds);
		setAttr("param", param.put("parentnames", parent.getStr("names")));
		render("/pingtai/param/update.html");
	}
	
	@Before(ParamValidator.class)
	public void update() {
		ParamService.service.update(getModel(Param.class));
		redirect("/jw/param/toUrl?toUrl=/pingtai/param/treeTableIframe.html", true);
	}
	
	public void delete() {
		ParamService.service.delete(getPara());
		redirect("/jw/param/toUrl?toUrl=/pingtai/param/treeTableIframe.html");
	}

	public void view() {
		Param param = Param.dao.findById(getPara());
		String pIds = param.getStr("parentids");
		Param parent = Param.dao.findById(pIds);
		setAttr("param", param.put("parentnames", parent.getStr("names")));
		render("/pingtai/param/view.html");
	}

}


