package com.gdmob.jweb.controller;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Controller;
import com.gdmob.jweb.service.ModuleService;
import com.gdmob.jweb.validator.ModuleValidator;
import com.jfinal.aop.Before;

@Controller(controllerKey = "/jw/module")
public class ModuleController extends BaseController {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(ModuleController.class);
	
	private String pIds;
	private String names;
	private int orderIds;
	
	public void index() {
		render("/pingtai/module/tree.html");
	}
	
	public void treeData()  {
		String jsonText = ModuleService.service.childNodeData(ids);
		renderJson(jsonText);
	}
	
	@Before(ModuleValidator.class)
	public void save() {
		ids = ModuleService.service.save(pIds, names, orderIds);
		renderText(ids);
	}
	
	@Before(ModuleValidator.class)
	public void update() {
		ModuleService.service.update(ids, pIds, names);
		renderText(ids);
	}
	
	public void delete() {
		ModuleService.service.delete(ids);
		renderText(ids);
	}

}


