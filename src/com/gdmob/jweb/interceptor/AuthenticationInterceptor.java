package com.gdmob.jweb.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.gdmob.jweb.controller.BaseController;
import com.gdmob.jweb.model.Operator;
import com.gdmob.jweb.model.User;
import com.gdmob.jweb.tools.ToolContext;
import com.gdmob.jweb.tools.ToolDateTime;
import com.gdmob.jweb.tools.ToolWeb;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;

/**
 * 权限认证拦截器
 */

public class AuthenticationInterceptor implements Interceptor {

	private static Logger log = Logger
			.getLogger(AuthenticationInterceptor.class);

	@Override
	public void intercept(ActionInvocation ai) {
		BaseController contro = (BaseController) ai.getController();
		HttpServletRequest request = contro.getRequest();
		HttpServletResponse response = contro.getResponse();
		String uri = ToolContext.getParam(request, "toUrl");
		if (null == uri || uri.equals("")) {
			uri = request.getServletPath();
			int index = uri.lastIndexOf("/");
			String deleteUri = uri.substring(index);
			if (deleteUri.length() == 33) {
				/**
				 * 去除最后的/ids 例如：tv/user/view/8a40c0353fa828a6013fa898d4ac0020
				 * 需要去除 /8a40c0353fa828a6013fa898d4ac0020
				 * */
				uri = uri.substring(0, index);
			}
		}
		// druid特殊处理
		if (uri.startsWith("/druid/")) {
			uri = "/pingtai/druid/iframe.html";
		}
		Object operatorObj = Operator.dao.cacheGet(uri);
		if (null != operatorObj) {
			Operator operator = (Operator) operatorObj;
			contro.setOperatorids(operator.getPrimaryKeyValue());
			if (operator.get("privilegess").equals("1")) {// 是否需要权限验证
				log.info("需要权限验证。");
				boolean userAgentVali = true;
				if (uri.equals("/jw/ueditor") || uri.equals("/jw/upload")) { // 针对ueditor特殊处理
					userAgentVali = false;
				}
				User user = ToolContext.getCurrentUser(request, userAgentVali);// 当前登录用户
				if (user == null) {
					toInfoJsp(contro, 1);
					return;
				}
				String userids = user.getPrimaryKeyValue();
				request.setAttribute("userids", userids);
				contro.setUserids(userids);
				if (!ToolContext.hasPrivilegeOperator(operator, user)) {// 权限验证
					log.info("权限验证失败，没有权限!");
					log.info("返回失败提示页面!");
					toInfoJsp(contro, 2);
					return;
				}
			}
			// 判断是否需要表单重复提交验证!
			if (operator.getStr("formtoken").equals("1")) {
				log.info("表单重复提交验证。");
				String tokenRequest = ToolContext
						.getParam(request, "formToken");
				String tokenCookie = ToolWeb.getCookieValueByName(request,
						"token");
				if (null == tokenRequest || tokenRequest.equals("")) {
					// tokenRequest为空，无需表单验证

				} else if (null == tokenCookie || tokenCookie.equals("")
						|| !tokenCookie.equals(tokenRequest)) {
					// tokenCookie为空，或者两个值不相等，把tokenRequest放入cookie
					ToolWeb.addCookie(response, "", "/", true, "token",
							tokenRequest, 0);
				} else if (tokenCookie.equals(tokenRequest)) {
					// 表单重复提交
					toInfoJsp(contro, 4);
					return;

				} else {
					log.error("表单重复提交验证异常!!!");
				}
			}
		}
		try {
			ai.invoke();
		} catch (Exception e) {

		}
	}

	/**
	 * 提示信息展示页
	 * 
	 * @param contro
	 * @param type
	 */
	private void toInfoJsp(BaseController contro, int type) {
		if (type == 1) {// 未登录处理
			contro.render("/login/logout");
			return;
		}

		String referer = contro.getRequest().getHeader("X-Requested-With");
		String toPage = "/common/msgAjax.html";
		if (null == referer || referer.isEmpty()) {
			toPage = "/common/msg.html";
		}

		String msg = null;
		if (type == 2) {// 权限验证失败处理
			msg = "权限验证失败!";

		} else if (type == 3) {// IP验证失败
			msg = "IP验证失败!";

		} else if (type == 4) {// 表单验证失败
			msg = "请不要重复提交表单数据!";

		} else if (type == 5) {// 业务代码异常
			msg = "业务代码异常!";
		}

		contro.setAttr("referer", referer);
		contro.setAttr("msg", msg);
		contro.render(toPage);
	}

}
